#ifndef __aula7__controlos__
#define __aula7__controlos__


#include <iostream>
#include <GLUT/glut.h>
#include <math.h>

extern  float raio,cam_h,cam_v,camh_x,camh_y;
extern  float x_tela, y_tela;

static int estado_botao=0;

void teclado_normal(unsigned char tecla,int x, int y);
void teclado_especial(int tecla,int x, int y);
void front_menu(int op);
void rato(int botao, int estado, int x, int y);
void mov_rato(int x, int y);

#endif