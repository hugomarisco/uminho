BEGIN             { FS=":"; conta=0 }
NR==1             { print "A processar o ficheiro: " FILENAME }
NR>=1 && NR<=10   { print $1 }
{ conta+=NF }
END               { print conta " - " NR  }

BEGIN              { FS=":"; conta=0 }
/rita/             { conta++; print $1 " -> " $6 }
/prh|jcr/          { conta++; print $1 " -> " $6 }
/uucp/,/rpm/       { conta++; print }
/x.*sbin/ && $3>40 { conta++; print "Em sbin: " $0 }
$1 ~ "nuno"        { conta++; print $1 " = " $NF }
END                { print conta " - " NR " = " conta/NR }
