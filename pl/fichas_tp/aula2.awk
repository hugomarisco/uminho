## HTML

# contar linhas começadas por 'linha'
BEGIN { conta=0 }
/^linha/ { conta++ }
END { print conta }

# linhas que contem marcas html
/<\/?HTML>/

# detectar links e imprimir o url
/<A[ \t]+HREF/ {
  split($0, bocados, /\"/);
  print "URL: " bocados[2];
}

# detectar links e imprimir o url e texto
/<A[ \t]+HREF/ {
  split($0, bocados, /[<>]/);
  print "TEXT: " bocados[3];
}

## INSCRITOS

# processa a segunda linha e chuta #campo \t designacao
BEGIN { FS="\t"; }
NR==2 { for(I=1; I<=NF; I++) print I "\t" $I; }

# imprimir nome e email do 5º ao 15º
BEGIN { FS="\t"; }
NR>=5 && NR<=15 { print $20 "\t" $28; }

# imprimir concorrentes individuais e sao de valongo
BEGIN { FS="\t"; }
$10 ~ /I[Nn][Dd]/ && $21 ~ /Valongo/ { print toupper($20); }
