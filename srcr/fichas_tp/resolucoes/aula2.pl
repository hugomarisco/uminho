max1(X, Y, X) :- X>Y.
max1(_, Y, Y).

max2(X, Y, X) :- X>Y.
max2(X, Y, Y) :- X=<Y.

max3(X, Y, X) :- X>Y, !.
max3(_, Y, Y).

pertence(X, [X|_]).
pertence(X, [_|T]) :- pertence(X, T).

comprimento([], 0).
comprimento([_|T], X) :- comprimento(T, X1), X is X1+1.

quantos([], 0).
quantos([H|T], N) :- pertence(H, T), quantos(T, N).
quantos([H|T], N) :- not(pertence(H, T)), quantos(T, N1), N is N1+1.

apagar(X, [X,T], T).
apagar(X, [H|T], [H|L]) :- X \== H, apagar(X, T, L).

con1(X, Y, [X|Y]).

con2(X, Y, [X,Y]).

concatenar( [], L2, L2).
concatenar( [X|R], L2, [X|L]) :- concatenar(R, L2, L).

/** <examples>

?- max1(1, 3, Z).
?- max2(2, 4, Z).
?- max3(3, 1, Z).
?- pertence(2, [1, 2, 3]).
?- pertence(4, [1, 2, 3]).
?- comprimento([1, 2, 3], X).
?- comprimento([1, 2, 3, 4, 5], X).
?- comprimento([], X).
?- quantos([2, 2, 3, 4, 5, 6, 6], X).
?- apagar(2, [2, 2, 3, 4, 5, 6, 6], X).
?- apagar(1, [2, 2, 3, 4, 5, 6, 6], X).
?- concatenar([1,2],[3,4], X).

*/
